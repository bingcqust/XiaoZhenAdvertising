package main

import (
	_ "AdvertisingProgram/routers"

	_ "AdvertisingProgram/models"

	"github.com/astaxie/beego"
)

func main() {
	beego.Run()
}
