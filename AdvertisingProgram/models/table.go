package models

// Titlebar 标题栏的二级标题
type Titlebar struct {
	Id       int       `json:"id"`
	Category *Category `orm:"rel(fk);null" json:"category"` // 一级标题 跟reverse成对出现
	Url      string    `json:"url"`                         // 二级标题对应的路径
	Title    string    `json:"title"`                       // 二级标题
}

// Category 标题栏的一级标题
type Category struct {
	Id        int
	Category  string
	Url       string      `json:"url"`          // 一级标题对应的路径
	Titlebars []*Titlebar `orm:"reverse(many)"` //跟rel成对出现
}

type Scrollimage struct {
	Id       int    `json:"id"`
	Dataarea string `json:"dataarea"`
	Url      string `json:"url"`
}
type Showenvironment struct {
	Id     int    `json:"id"`
	Delay  string `json:"delay"`
	Left   string `json:"left"`
	Top    string `json:"top"`
	Rotate string `json:"rotate"`
	Class  string `json:"class"`
	Image  string `json:"image"`
}

type Aim struct {
	Id       int
	Class1   string
	Class2   string
	Delay    string
	Height1  string
	Width1   string
	Left1    string
	Top1     string
	Zindex1  string
	Height2  string
	Width2   string
	Left2    string
	Top2     string
	Zindex2  string
	Textsize string
	Text     string
	English  string
}

type ShowPictures struct {
	Id       int
	Category string // 类型
	Image    string //图片
	Text     string //文本描述
}
