package models

import (
	"github.com/astaxie/beego/logs"
	"github.com/astaxie/beego/orm"
	_ "github.com/go-sql-driver/mysql"
)

func init() {
	// 1 连接数据库
	err := orm.RegisterDataBase("default", "mysql", "root:password@tcp(127.0.0.1:3306)/advertisingProgram?charset=utf8&loc=Local")
	if err != nil {
		logs.Info("连接数据库失败")
		return
	}
	//2 建表
	orm.RegisterModel(new(Titlebar), new(Category), new(Scrollimage), new(Showenvironment), new(Aim), new(ShowPictures))
	err = orm.RunSyncdb("default", false, true)
	if err != nil {
		logs.Info("创建表失败")
		return
	}
}
