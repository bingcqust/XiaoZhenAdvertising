package controllers

import (
	"AdvertisingProgram/models"
	"github.com/astaxie/beego"
	"github.com/astaxie/beego/orm"
)

type MainController struct {
	beego.Controller
}

func (c *MainController) Get() {
	// 1 orm对象
	o := orm.NewOrm()
	// 2 获取标题栏分内容数据
	// 2.1 对象切片
	var categories []models.Category
	// 2.2 查询所有内容
	_, err := o.QueryTable("Category").All(&categories)
	if err != nil {
		return
	}
	// 2.3 循环获取一对多查询
	for i := 0; i < len(categories); i++ {
		_, err = o.LoadRelated(&categories[i], "Titlebars")
		if err != nil {
			return
		}
	}
	// 3 获取滚动栏除了第一张图之外的所有数据
	// 3.1 对象切片
	var scrollImage []models.Scrollimage
	// 3.2 查询所有内容
	_, err = o.QueryTable("Scrollimage").All(&scrollImage)

	// 4 获取办公室环境图片
	// 4.1 对象切片
	var showEnvironments []models.Showenvironment
	// 4.2 查询
	_, err = o.QueryTable("Showenvironment").All(&showEnvironments)

	// 5 获取公司目标数据
	// 5.1 对象切片
	var aim []models.Aim
	// 5.2 查询
	_, err = o.QueryTable("Aim").All(&aim)

	// 6 获取多组展示图片
	// 6.1 对象切片
	var pictures1 []models.ShowPictures
	var pictures2 []models.ShowPictures

	// 6.2 查询
	_, err = o.QueryTable("ShowPictures").Filter("Category", "A").All(&pictures1)
	_, err = o.QueryTable("ShowPictures").Filter("Category", "B").All(&pictures2)

	// 5 传递数据
	c.Data["categories"] = categories
	c.Data["scrollImage"] = scrollImage
	c.Data["ShowEnvironments"] = showEnvironments
	c.Data["aim"] = aim
	c.Data["pictures1"] = pictures1
	c.Data["pictures2"] = pictures2

	// 6 业务
	c.TplName = "in.html"
}

func (c *MainController) Test() {

}
